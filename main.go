package main

import (
	"github.com/pborman/getopt"
	"gitlab.com/Alvoras/webhook/internal/config"
	"gitlab.com/Alvoras/webhook/internal/logger"
	"gitlab.com/Alvoras/webhook/internal/router"
)

func init() {
	config.UseStdout = getopt.BoolLong("stdout", 'P', "true")
	config.Port = getopt.IntLong("port", 'p', 65000)
	getopt.Parse()

	logger.InitHTTPLogging()
	logger.InitEventLogging()
}

func main() {
	router.Up()
}
