package logger

import (
	"io"
	"log"
	"os"

	. "github.com/logrusorgru/aurora"
	"gitlab.com/Alvoras/webhook/internal/config"
)

var (
	Info    *log.Logger
	Warning *log.Logger
	Error   *log.Logger
	Success *log.Logger
	HTTP    *log.Logger
	Event   *log.Logger
)

func init() {
	var infoWriter io.Writer = os.Stdout
	var warningWriter io.Writer = os.Stdout
	var errorWriter io.Writer = os.Stdout
	var successWriter io.Writer = os.Stdout

	Success = log.New(successWriter,
		Bold(Green("[+] ")).String(),
		0,
	)

	Info = log.New(infoWriter,
		Bold("[i] ").String(),
		0,
	)

	Warning = log.New(warningWriter,
		Bold(Yellow("[!] ")).String(),
		0,
	)

	Error = log.New(errorWriter,
		Bold(Red("[x] ")).String(),
		0,
	)
}

func InitHTTPLogging() {
	var HTTPWriter io.Writer
	f, err := os.OpenFile("logs/http.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)

	if *config.UseStdout {
		Info.Println("HTTP requests will be sent to stdout")
		HTTPWriter = os.Stdout
	} else if err != nil {
		Warning.Println(err)
		Warning.Println("Failed to open the HTTP log file. Defaulting to stdout")
		HTTPWriter = os.Stdout
	} else {
		HTTPWriter = f
	}

	HTTP = log.New(HTTPWriter,
		"",
		log.Ldate|log.Ltime,
	)
}

func InitEventLogging() {
	var EventWriter io.Writer
	f, err := os.OpenFile("logs/events.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)

	if *config.UseStdout {
		Info.Println("Events will be sent to stdout")
		EventWriter = os.Stdout
	} else if err != nil {
		Warning.Println(err)
		Warning.Println("Failed to open the event log file. Defaulting to stdout")
		EventWriter = os.Stdout
	} else {
		EventWriter = f
	}

	Event = log.New(EventWriter,
		"",
		log.Ldate|log.Ltime,
	)
}
