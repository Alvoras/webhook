package router

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/Alvoras/webhook/internal/config"
	"gitlab.com/Alvoras/webhook/internal/logger"
)

var (
	Router *mux.Router
)

func init() {
}

func loggerFn(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger.HTTP.Printf("[%s] - %s", r.Method, r.RequestURI)
		next.ServeHTTP(w, r)
	})
}

func Up() {
	Router = mux.NewRouter()
	Router.Handle("/", http.HandlerFunc(SendOK)).Methods("GET", "POST", "HEAD", "PUT")
	Router.Handle("/newfile", http.HandlerFunc(NewFile)).Methods("POST")

	Router.Use(loggerFn)

	server := &http.Server{
		Addr:         fmt.Sprintf(":%d", *config.Port),
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      Router,
	}

	logger.Info.Println("Starting HTTP server...")
	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		logger.Error.Println("Failed to start the server :", err)
		os.Exit(1)
	}
}
