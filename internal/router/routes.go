package router

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/Alvoras/webhook/internal/logger"
)

func SendOK(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func NewFile(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	resJSON := &struct {
		Filename string `json:"Filename"`
		Content  string `json:"Content"`
		OpName   string `json:"OpName"`
	}{}

	json.Unmarshal(body, resJSON)

	logger.Event.Println(fmt.Sprintf("Filename : %s\nContent : %s\nOpName : %s\n---", resJSON.Filename, resJSON.Content, resJSON.OpName))
}
